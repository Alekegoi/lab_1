# frozen_string_literal: true

# Pet class
class Pet
  attr_accessor :name, :age, :gender, :color
end

# Cat class
class Cat < Pet
end

# Snake class
class Snake < Pet
  attr_accessor :length
end

# Dog class
class Dog < Pet
  def bark(number)
    number.times { puts 'Woof!' }
  end
end
